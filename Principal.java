package org.inguelberth.sistema;

import org.inguelberth.utilidades.Entrada;
import org.inguelberth.manejadores.ManejadorUsuario;
import org.inguelberth.app.AppAdmin;
import org.inguelberth.app.AbstractAppRol;

public class Principal{
	public void iniciar(){
		ManejadorUsuario manejador = new ManejadorUsuario();
		AbstractAppRol app = null;		


		String nick, password;
		System.out.println("Ingrese Nick:");
		nick=Entrada.getInstancia().leer();
		System.out.println("Ingrese Password:");
		password=Entrada.getInstancia().leer();
		
		boolean resultado = manejador.autenticarUsuario(nick, password);

		if(resultado){
			System.out.println("Bienvenido "+manejador.obtenerUsuarioAutenticado().getNombre());
			switch(manejador.obtenerUsuarioAutenticado().getRol()){
				case "admin":
					app=new AppAdmin();
					app.iniciar();
					break;
				default:
					System.out.println("Nuestro sistema esta en mantenimiento, su rol no existe. :(");
			}
		}else
			System.out.println("Verifique sus credenciales.");
	}
}
